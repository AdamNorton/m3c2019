"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt

def brown1(Nt,M,dt=0.0001):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    #Initialize variable
    X = np.zeros((M,Nt+1))

    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    for i in range(M):
        for j in range(Nt):
            X[i,j+1] = X[i,j] + np.sqrt(dt)*randn(1)

    Xm = X.mean(axis=0)
    Xv = X.var(axis=0)

    return X,Xm,Xv


def analyze(display=False):
    Mvalues = np.arange(1,1000) #np.array([25,50,75,100,250,500,750,1000,1500,2000,2500])
    errors = np.zeros(Mvalues.size)
    varrs = np.zeros(Mvalues.size)
    dt = 0.0001
    for i in range(Mvalues.size):
        Xvtj = brown1(100,Mvalues[i],dt)[2][100]
        errors[i] =  abs(100*dt-Xvtj)
        varrs[i] = Xvtj
    if display==True:
        plt.loglog(Mvalues,errors); plt.show()
        #plt.plot(Mvalues,np.log(errors)); plt.show()
    return  Mvalues, errors,varrs
    """Complete this function to analyze simulation error
    """
